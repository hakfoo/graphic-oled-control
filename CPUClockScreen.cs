using LibreHardwareMonitor.Hardware;

/* Uses UpdateVisitor class from the LibreHardwareMonitor project example
https://github.com/LibreHardwareMonitor/LibreHardwareMonitor - see LICENSE-for-librehardwaremonitor-library file
Data extraction also based on example shown on there.
*/

class CPUClockScreen : DisplayScreen
{
    public CPUClockScreen()
    {
    }

    public override void RenderScreen(DisplayModule display, int currentFrame)
    {
        int cores = 0;
        float mhz = 0;
        float min = 999999999;
        float max = 0;
        Computer computer = HardwareSensor.getComputer();
        foreach (IHardware hardware in computer.Hardware) {
            if (hardware.HardwareType == HardwareType.Cpu) {
                hardware.Update();
                foreach (ISensor sensor in hardware.Sensors) {
                    // This is sort of terrible, in that there may be clocks OTHER than core clocks affecting average/min/max
                    if (sensor.SensorType == SensorType.Clock && sensor.Name != "Bus Speed") {
                        cores++;
                        mhz += (float)(sensor.Value ?? 0);
                        if ((float)(sensor.Value ?? 0) > max) {
                            max = (float)(sensor.Value ?? 0);
                        }
                        if ((float)(sensor.Value ?? 0) < min) {
                            min = (float)(sensor.Value ?? 0);
                        }
                    }
                }
            }
        }
        display.clearScreen();
        display.setFont(123);
        display.setPrecisePosition(10, 40);
        display.writeText(((int)max).ToString());
        display.setFont(10);
        String summary = "Min: " + ((int)min).ToString() + " - Avg: " + ((int)(mhz/cores)).ToString();
        display.setPrecisePosition(PrettyNumbers.centeredOffset(summary, 6), 56);
        display.writeText(summary);
        display.setFont(0);
        display.dumpBuffer();
    }

    public override String getName()
    {
        return "CPU Clock";
    }
}