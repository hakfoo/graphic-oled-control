using System.Diagnostics;
using System.Management;
class CPUScreen : DisplayScreen {
    private Dictionary<String, PerformanceCounter> cpuCounters = new Dictionary<String, PerformanceCounter>();
    private PerformanceCounter memoryCounter;
    private ulong memory;
    private int coreCount = 0;
    private Byte barWidth = 1;
    public CPUScreen()
    {
        PerformanceCounterCategory category = new PerformanceCounterCategory("Processor");
        String[] names = category.GetInstanceNames();
        foreach (string name in names) {
            cpuCounters.Add(name, new PerformanceCounter("Processor", "% Processor Time", name));
            if (name != "_Total") {
                coreCount++;
            }
        }
        barWidth = (Byte)((128 / coreCount) - 2);
        memoryCounter = new PerformanceCounter("Memory", "Available Bytes");
        memory = findMemory();
        
    }

    private ulong findMemory()
    {
        ulong totalMemory = 0;
        ObjectQuery query;
        ManagementObjectCollection results;
        ManagementObjectSearcher searcher;
        query = new ObjectQuery("SELECT * FROM Win32_ComputerSystem");
        searcher = new ManagementObjectSearcher(query);
        results = searcher.Get();
        foreach (ManagementObject obj in results) {
            totalMemory = (ulong) obj["TotalPhysicalMemory"];
        }
        return totalMemory;
    }

    public override void RenderScreen(DisplayModule display, int currentFrame)
    {
        float cpuUsage = cpuCounters["_Total"].NextValue();
        ulong memoryUsage = memory - (ulong)memoryCounter.NextValue();
        display.clearScreen();
        display.writeText(String.Format("CPU:     {0,6:F2}%Memory:{1,8}", cpuUsage, PrettyNumbers.formatNumber(memoryUsage,8)));
        
        display.drawLine(0, 32, 127, 32);
        display.drawLine(0, 63, 127, 63);
        Byte basex = (Byte)(64 - (coreCount * (barWidth + 2)) / 2);
        float percentage = 0;
        foreach (KeyValuePair<String, PerformanceCounter> kvp in cpuCounters.OrderBy(x => x.Key)) {
            if (kvp.Key != "_Total") {
                basex++;
                percentage = kvp.Value.NextValue();
                display.drawFilledRectangle(basex, (Byte) (62-(30 * percentage / 100)), (Byte)(basex + barWidth), 62);
                basex += barWidth;
                basex++;
                
            }
        }
        display.dumpBuffer();
    }

    public override String getName()
    {
        return "CPU/RAM";
    }   
}