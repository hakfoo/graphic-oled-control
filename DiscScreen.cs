using System.Diagnostics;
using System.Management;
class DiscScreen : DisplayScreen {
    
    public DiscScreen()
    {
        
    }

    private Dictionary<ulong, ManagementObject> getDiscs()
    {
        Dictionary<ulong, ManagementObject> discs = new Dictionary<ulong, ManagementObject>();
        ObjectQuery query;
        ManagementObjectCollection results;
        ManagementObjectSearcher searcher;
        query = new ObjectQuery("SELECT * FROM Win32_LogicalDisk where Size is not null");
        searcher = new ManagementObjectSearcher(query);
        results = searcher.Get();
        foreach (ManagementObject obj in results) {
            char drive = ((string)obj["Name"]).ToCharArray()[0];
            ulong key = (ulong)obj["Size"] * 0x100 + drive; // So two drives of equal size get seperate keys
            discs.Add(key, obj);
        }
        return discs;
    }

    private Byte[][] icons = new Byte[][] {
        // Type 0, unknown
        new Byte[] {
          0b00000000,0b00000000,  
          0b00011111,0b11111000,   
          0b01111111,0b11111110,   
          0b11110000,0b00001111,
          0b00000000,0b00001111,
          0b00000000,0b00011110,
          0b00000000,0b00111100,
          0b00000000,0b01111000,
          0b00000000,0b11110000,
          0b00000001,0b11100000,
          0b00000011,0b11000000,
          0b00000011,0b11000000,
          0b00000000,0b00000000,
          0b00000011,0b11000000,
          0b00000011,0b11000000,
          0b00000000,0b00000000,
        },
        // Type 1, No root
        new Byte[] {
          0b00000000,0b00000000,  
          0b11110000,0b00001111,   
          0b01111000,0b00011110,
          0b01111000,0b00011110,
          0b00111100,0b00111100,
          0b00011110,0b01111000,
          0b00011110,0b01111000,
          0b00001111,0b11110000,
          0b00001111,0b11110000,
          0b00011110,0b01111000,
          0b00011110,0b01111000,
          0b00111100,0b00111100,
          0b01111000,0b00011110,
          0b01111000,0b00011110,
          0b11110000,0b00001111,   
          0b00000000,0b00000000,
        },
        // Type 2, Removable
        new Byte[] {
          0b00000000,0b00000000,  
          0b11111111,0b11111111,   
          0b10100000,0b00000101,   
          0b10100000,0b00000101,
          0b10100000,0b00000101,
          0b10011111,0b11111001,
          0b10000000,0b00000001,
          0b10000000,0b00000001,
          0b10001111,0b11110001,
          0b10010000,0b00001001,
          0b10010110,0b00001001,
          0b10010110,0b00001001,
          0b10010110,0b00001001,
          0b01010000,0b00001001,
          0b00111111,0b11111111,
          0b00000000,0b00000000,
        },
        // Type 3, Local
        new Byte[]{
          0b00000000,0b00000000,  
          0b00001111,0b11110000,   
          0b11110000,0b00001111,   
          0b10000000,0b00000001,
          0b10000000,0b00000001,
          0b11110000,0b00001111,
          0b10001111,0b11110001,
          0b10000000,0b00000001,
          0b10000000,0b00000001,
          0b10000000,0b00000001,
          0b10000000,0b00000001,
          0b10000000,0b00000001,
          0b10000000,0b00000001,
          0b11110000,0b00001111,
          0b00001111,0b11110000,
          0b00000000,0b00000000,
        },
        // Type 4, Network
        new Byte[]{
          0b00000000,0b00000000,  
          0b11100000,0b00000111,   
          0b11110000,0b00000111,
          0b11111000,0b00000111,
          0b11111100,0b00000111,
          0b11101110,0b00000111,
          0b11100111,0b00000111,
          0b11100011,0b10000111,
          0b11100001,0b11000111,
          0b11100000,0b11100111,
          0b11100000,0b01110111,
          0b11100000,0b00111111,
          0b11100000,0b00011111,
          0b11100000,0b00001111,   
          0b11100000,0b00000111,   
          0b00000000,0b00000000,
        },
        // Type 5, CD
        new Byte[]{
          0b00000000,0b00000000,  
          0b00000011,0b11000000,   
          0b00011100,0b00111000,   
          0b01100000,0b00000110,
          0b11000000,0b00000011,
          0b10000000,0b00000001,
          0b10000111,0b11100001,
          0b10001000,0b00010001,
          0b10001000,0b00010001,
          0b10000111,0b11100001,
          0b10000000,0b00000001,
          0b11000000,0b00000011,
          0b01100000,0b00000110,
          0b00011100,0b00111000,
          0b00000011,0b11000000,
          0b00000000,0b00000000,
        },
        // Type 6, RAM disc
        new Byte[]{
          0b00000000,0b00000000,  
          0b11111111,0b11100000,   
          0b11111111,0b11111000,
          0b11100000,0b00011100,
          0b11100000,0b00011100,
          0b11100000,0b00011100,
          0b11100000,0b01111000,
          0b11111111,0b11100000,
          0b11111111,0b11000000,
          0b11100000,0b11100000,
          0b11100000,0b01110000,
          0b11100000,0b00111000,
          0b11100000,0b00011100,
          0b11100000,0b00001110,   
          0b11100000,0b00000111,   
          0b00000000,0b00000000,
        },
    };

    public override void RenderScreen(DisplayModule display, int currentFrame)
    {
        if (currentFrame % 15 != 0) {
          return;
        }
        Dictionary<ulong, ManagementObject> discs = getDiscs();
        int yOffset = 1;
        UInt32 driveType = 0;
        display.clearScreen();
        foreach (var obj in discs.OrderByDescending(x => x.Key)) {
            if (yOffset > 3) {
                continue;
            }
            display.setFont(51);
            display.setPrecisePosition(30, (byte)(21 * yOffset));
            display.writeText((String)((ManagementObject)(obj.Value))["Name"]);
            display.setFont(10);
            driveType = (UInt32)(((ManagementObject)(obj.Value))["DriveType"]);
            if (icons[driveType].Length == 32) {
                display.drawImage(0, (byte) (21 * yOffset - 17), 16, 16, icons[driveType]);
            }
            display.setPrecisePosition(72, (byte)(21 * yOffset - 10));
            display.writeText(PrettyNumbers.formatNumber((ulong)((ManagementObject)(obj.Value))["Size"] - (ulong)((ManagementObject)(obj.Value))["FreeSpace"]));
            display.setPrecisePosition(72, (byte)(21 * yOffset));
            display.writeText(PrettyNumbers.formatNumber((ulong)((ManagementObject)(obj.Value))["Size"]));
            display.setFont(0);
            yOffset++;
        }
        display.dumpBuffer();
    }

    public override String getName()
    {
        return "Discs";
    }   
}