using System.IO.Ports;
using System.Text;

public class DisplayModule
{
    List<Byte> displayBuffer = new List<Byte>();
    private SerialPort port;
    public DisplayModule(String portName)
    {
        port = new SerialPort(portName);
    }

    public void initialize()
    {
        // Step 1, try to initialize port
        port.BaudRate = 9600;
        port.Parity = Parity.None;
        port.StopBits = StopBits.One;
        port.DataBits = 8;
        port.Handshake = Handshake.None;
        port.Open();
        port.Write("SB115200");
        port.Close();

        port.BaudRate = 115200;
        port.Parity = Parity.None;
        port.StopBits = StopBits.One;
        port.DataBits = 8;
        port.Handshake = Handshake.None;
        port.Open();
        clearScreen();
        turnOnScreen();
        Byte[] image = new Byte[] {
            0b00011000,0b00011000,
            0b00111100,0b00111100,
            0b01111110,0b01111110,
            0b01111111,0b11111110,
            0b01111111,0b11111110,
            0b01111111,0b11111110,
            0b01111111,0b11111110,
            0b01111111,0b11111110,
            0b01111111,0b11111110,
            0b00111111,0b11111100,
            0b00111111,0b11111100,
            0b00001111,0b11110000,
            0b00001111,0b11110000,
            0b00000011,0b11000000,
            0b00000011,0b11000000,
            0b00000001,0b10000000,
        };
        drawImage(48, 0, 16, 16, image);
        setPosition(0, 2);
        writeText("Startup text");
        dumpBuffer();
    }

    public void addToBuffer(String body)
    {
        Byte[] ascii = Encoding.ASCII.GetBytes(body);
        foreach (Byte character in ascii) {
            displayBuffer.Add(character);
        }
    }

    public void addByteToBuffer(Byte character)
    {
        displayBuffer.Add(character);
    }

    public void addRawToBuffer(Byte[] ascii)
    {
        foreach (Byte character in ascii) {
            displayBuffer.Add(character);
        }
    }

    public void turnOffScreen()
    {
        addToBuffer("SOO0");
        dumpBuffer();
    }

    public void turnOnScreen()
    {
        addToBuffer("SOO1");
    }
    public void clearScreen()
    {
        addToBuffer("CL");
    }

    public void writeText(String unicodeString)
    {
        addToBuffer("TT" + unicodeString);
        addByteToBuffer(0); 
    }

    public void setFont(Byte font)
    {
        Byte[] command = new Byte[] {(Byte)'S', (Byte)'F', font};
        addRawToBuffer(command);
    }

    public void setPosition(Byte x, byte y)
    {
        Byte[] command = new Byte[] {(Byte)'T', (Byte)'P', x, y};
        addRawToBuffer(command);
    }

    public void setPrecisePosition(Byte x, byte y)
    {
        Byte[] command = new Byte[] {(Byte)'E', (Byte)'T', (Byte)'P', x, y};
        addRawToBuffer(command);
    }

    public void dumpBuffer()
    {
        Byte[] bufferArray = displayBuffer.ToArray();        
        port.Write(bufferArray, 0, bufferArray.Length);
        displayBuffer = new List<byte>();
    }

    public void drawLine(Byte x1, Byte y1, Byte x2, Byte y2)
    {
        Byte[] command = {(Byte)'L', (Byte)'N', x1, y1, x2, y2};
        addRawToBuffer(command);
    }

    public void drawFilledRectangle(Byte x1, Byte y1, Byte x2, Byte y2)
    {
        Byte[] command = {(Byte)'F', (Byte)'R', x1, y1, x2, y2};
        addRawToBuffer(command);
    }

    public void drawImage(Byte x, Byte y, Byte w, Byte h, Byte[] data)
    {
        Byte[] command = {(Byte)'D', (Byte)'I', (Byte)'M', x, y, w, h};
        addRawToBuffer(command);
        addRawToBuffer(data);
    }
}