class DisplayScreen
{
    public virtual void RenderScreen(DisplayModule display, int currentFrame)
    {
        display.clearScreen();
        display.writeText("Placeholder screen not yet implemented");
        display.dumpBuffer();
    }
    public virtual String getName()
    {
        return "Placeholder Screen";
    }

    public virtual int getLifespan()
    {
        return 30;
    }
}