using LibreHardwareMonitor.Hardware;

/* Uses UpdateVisitor class from the LibreHardwareMonitor project example
https://github.com/LibreHardwareMonitor/LibreHardwareMonitor - see LICENSE-for-librehardwaremonitor-library file
Data extraction also based on example shown on there.
*/

class GPUScreen : DisplayScreen
{
    private int index;
    public GPUScreen(int index)
    {
        this.index = index;
    }


    public override void RenderScreen(DisplayModule display, int currentFrame)
    {
        Computer computer = HardwareSensor.getComputer();
        Dictionary<String, float?> fans = new Dictionary<string, float?> {};
        Dictionary<String, float?> clocks = new Dictionary<string, float?> {};
        Dictionary<String, float?> temperatures = new Dictionary<string, float?> {};
        int counter = 0;
        int gpuCounter = 0;
        String gpuName = "Name not known";
        Console.WriteLine(this.index);
        foreach (IHardware hardware in computer.Hardware) {
            if (hardware.HardwareType == HardwareType.GpuAmd || hardware.HardwareType == HardwareType.GpuNvidia) {
                Console.WriteLine(gpuCounter+"/"+this.index);
                if (this.index == gpuCounter) {
                    gpuName = hardware.Name;
                    foreach (ISensor sensor in hardware.Sensors) {
                        Console.WriteLine(sensor.Name+"/"+sensor.SensorType);
                        String name = sensor.Name;
                        if (sensor.SensorType == SensorType.Clock) {
                            if (clocks.ContainsKey(name)) {
                                counter = 2;
                                while (clocks.ContainsKey(name + counter))
                                    counter ++;
                                name = name + counter;
                            }
                            clocks.Add(name, sensor.Value);
                        }
                        if (sensor.SensorType == SensorType.Temperature) {
                            if (temperatures.ContainsKey(name)) {
                                counter = 2;
                                while (temperatures.ContainsKey(name + counter))
                                    counter ++;
                                name = name + counter;
                            }
                            temperatures.Add(name, sensor.Value);
                        }
                        if (sensor.SensorType == SensorType.Fan) {
                            if (fans.ContainsKey(name)) {
                                counter = 2;
                                while (fans.ContainsKey(name + counter))
                                    counter ++;
                                name = name + counter;
                            }
                            fans.Add(name, sensor.Value);
                        }
                    }
                }
                gpuCounter++;
            }
        }
        display.clearScreen();
        byte mainOffset = 0;
        if (fans.Count == 0 && temperatures.Count == 0 && clocks.Count == 0) {
            display.setFont(18);
            display.setPosition(0,0);
            display.writeText(gpuName);
            mainOffset++;
            display.setPosition(2,2);
            display.setFont(0);
            display.writeText("No Sensors.");
        }
        display.setFont(0);
        
        foreach (var obj in clocks.OrderBy(x => x.Key)) {
            display.setPosition(0, mainOffset);
            String caption = obj.Key.Length > 8 ? obj.Key.Substring(0, 8) : obj.Key;
            display.writeText(String.Format("{0,-8} {1,4:F0}MHz", caption, obj.Value));
            mainOffset++;
        }
        display.setFont(10);
        byte offset = 0;
        foreach (var obj in temperatures.OrderByDescending(x => x.Value)) {
            if ((4 + mainOffset * 16 + offset * 10) > 63) break;
            display.setPrecisePosition(0, (byte)(4 + mainOffset * 16 + offset * 10));
            String caption = obj.Key.Length > 6 ? obj.Key.Substring(0, 6) : obj.Key;
            display.writeText(String.Format("{0,-6} {1,3:F0}c", caption, obj.Value));
            offset++;
        }
        offset = 0;
        foreach (var obj in fans.OrderByDescending(x => x.Value)) {
            if ((4 + mainOffset * 16 + offset * 10) > 63) break;
            display.setPrecisePosition(72, (byte)(4 + mainOffset * 16 + offset * 10));
            String caption = obj.Key.Length > 4 ? obj.Key.Substring(0, 4) : obj.Key;
            display.writeText(String.Format("{0,-4} {1,4:F0}", caption, obj.Value));
            offset++;
        }

        display.setFont(0);
        display.dumpBuffer();
    }

    public override String getName()
    {
        return "GPU " + this.index + " Summary";
    }
}