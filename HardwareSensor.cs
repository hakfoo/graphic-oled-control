using LibreHardwareMonitor.Hardware;

/* Make a singleton for the monitor object so we don't have a bunch of them open
Follows code from the LibreHardwareMonitor project example
https://github.com/LibreHardwareMonitor/LibreHardwareMonitor - see LICENSE-for-librehardwaremonitor-library file
 */
static class HardwareSensor {
    static Computer? computer = null;

    public static Computer getComputer() {
        if (computer == null) {
            computer = new Computer {
                IsCpuEnabled = true,
                IsMotherboardEnabled = true,
                IsGpuEnabled = true,
            };
            computer.Open();
            computer.Accept(new UpdateVisitor());
        }
        return computer;
    }
}