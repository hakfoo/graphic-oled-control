using System.Management;
class NetworkScreen : DisplayScreen
{
    private String ip = "";
    private String cardName = "";
    private long lastChecked = ((DateTimeOffset)DateTime.UtcNow).ToUnixTimeMilliseconds();
    private ulong lastUp = 0;
    private ulong lastDown = 0;
    private ManagementObject? counter = null;

    

    private Byte[] downArrow = new Byte[] {
        0b00011000,
        0b00011000,
        0b00011000,
        0b00011000,
        0b00011000,
        0b00011000,
        0b00011000,
        0b00011000,
        0b11111111,
        0b01111110,
        0b00111100,
        0b00011000,
    };

    private Byte[] upArrow = new Byte[] {
        0b00011000,
        0b00111100,
        0b01111110,
        0b11111111,
        0b00011000,
        0b00011000,
        0b00011000,
        0b00011000,
        0b00011000,
        0b00011000,
        0b00011000,
        0b00011000,
    };
    public NetworkScreen()
    {
        findNetworkCounter();
        getStats();
        findIp();
    }

    private void findNetworkCounter()
    {
        ObjectQuery query;
        ManagementObjectCollection results;
        ManagementObjectSearcher searcher;
        query = new ObjectQuery("SELECT * FROM Win32_PerfRawData_Tcpip_NetworkInterface");
        searcher = new ManagementObjectSearcher(query);
        results = searcher.Get();
        foreach (ManagementObject obj in results) {
            if ((ulong)obj["BytesReceivedPersec"] > 0 || (ulong)obj["BytesSentPersec"] > 0) {
            counter = obj;
            cardName = (String)obj["Name"];
            }
        }

    }

    private void findIp()
    {
        ObjectQuery query;
        ManagementObjectCollection results;
        ManagementObjectSearcher searcher;
        query = new ObjectQuery("SELECT * FROM Win32_NetworkAdapterConfiguration");
        searcher = new ManagementObjectSearcher(query);
        results = searcher.Get();
        foreach (ManagementObject obj in results) {
            object ipList = obj["IPAddress"];
            object gateway = obj["DefaultIPGateway"];
            if (ipList is not null && gateway is not null) {
                ip = ((String[])ipList)[0];
            }   
        }
    }

    private Dictionary<String, ulong> getStats()
    {
        ulong down = 0;
        ulong up = 0;
        if (counter is not null) {
            counter.Get();                
            down =(ulong)counter["BytesReceivedPersec"];
            up = (ulong)counter["BytesSentPersec"];
        }
        Dictionary<String, ulong> stats = new Dictionary<String, ulong> ();
        stats.Add("up", up);
        stats.Add("down", down);
        double sinceLast = (((double)((DateTimeOffset)DateTime.UtcNow).ToUnixTimeMilliseconds()) - (double)lastChecked) / (double)1000;
        if (sinceLast > 0) {
            stats.Add("downRate", (ulong)((down - lastDown) / sinceLast));
            stats.Add("upRate", (ulong)((up - lastUp) / sinceLast));
        } else {
            stats.Add("downRate", 0);
            stats.Add("upRate", 0);
        }
        
        lastUp = up;
        lastDown = down;
        lastChecked = ((DateTimeOffset)DateTime.UtcNow).ToUnixTimeMilliseconds();
        return stats;
    }
    public override void RenderScreen(DisplayModule display, int currentFrame)
    {
        if (currentFrame == 0) {
            findIp();
        }
        display.clearScreen();
        display.setPrecisePosition(PrettyNumbers.centeredOffset(ip), 12);
        display.writeText(ip);
        Dictionary<String, ulong> stats = getStats();
        display.drawImage(0, 18, 8, 12, downArrow);
        
        display.setFont(10);
        display.setPrecisePosition(8, 22);
        display.writeText(PrettyNumbers.formatNumber(stats["down"]));
        display.setPrecisePosition(8, 32);
        display.writeText(PrettyNumbers.formatNumber(stats["downRate"],5) + "/s");
        display.setPrecisePosition(72, 22);
        display.writeText(PrettyNumbers.formatNumber(stats["up"]));
        display.setPrecisePosition(72, 32);
        display.writeText(PrettyNumbers.formatNumber(stats["upRate"],5)+ "/s");
        display.setFont(0);
        display.setPrecisePosition(0, 48);
        display.writeText(cardName);
        display.drawImage(60, 18, 8, 12, upArrow);
        display.dumpBuffer();
    }

    public override String getName()
    {
        return "Network";
    }
}