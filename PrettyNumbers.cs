class PrettyNumbers
{
   public static String formatNumber(ulong number, int length = 7)
    {
        // Integer values have 1 character less than length to work with
        // Floating point has two less.
        if (number > 10736344498176) { // Over 9999G -- value is NNNN.NNT or
            return String.Format("{0," + length+":F" + Math.Max(length - 6, 0) + "}T", (Decimal)number/1099511627776);
        }
        if (number > 1072668082176) { // Over 999G value is NNNN.NNG
            return String.Format("{0," + length+":F" + Math.Max(length - 6, 0) + "}G", (Decimal)number/(1024*1024*1024));
        }
        if (number > 106300440576) { // Over 99G value is NNN.NNNG
            return String.Format("{0," + length+":F" + Math.Max(length - 5, 0) + "}G", (Decimal)number/(1024*1024*1024));
        }
        if (number > 9663676416) { // Over 9G value is NN.NNNNG
            return String.Format("{0," + length+":F" + Math.Max(length - 4, 0) + "}G", (Decimal)number/(1024*1024*1024));
        }
        
        if (number > 10484711424) { // Over 9999M value is N.NNNNNG
            return String.Format("{0," + length+":F" + Math.Max(length - 3, 0) + "}G", (Decimal)number/(1024*1024*1024));
        }

        if (number > 1047527424) { // Over 999M value is NNNN.NNM
            return String.Format("{0," + length+":F" + Math.Max(length - 6, 0) + "}M", (Decimal)number/(1024*1024));
        }
        if (number > 103809024) { // Over 99M value is NNN.NNNM
            return String.Format("{0," + length+":F" + Math.Max(length - 5, 0) + "}M", (Decimal)number/(1024*1024));
        }
        if (number > 9437184) { // Over 9M value is NN.NNNNM
            return String.Format("{0," + length+":F" + Math.Max(length - 4, 0) + "}M", (Decimal)number/(1024*1024));
        }

        if (number > 9999*1024) { // Over 9999k, value is N.NNNNNM
            return String.Format("{0," + length+":F" + Math.Max(length - 3, 0) + "}M", (Decimal)number/(1024*1024));
        }
        if (number > 999*1024) { // Over 999k, value is NNNN.NNK
            return String.Format("{0," + length+":F" + Math.Max(length - 6, 0) + "}K", (Decimal)number/(1024));
        }

        if (number > 99*1024) { // Over 99k, value is NNN.NNNK
            return String.Format("{0," + length+":F" + Math.Max(length - 5, 0) + "}K", (Decimal)number/(1024));
        }

        if (number > 9*1024) { // Over 9k, value is NN.NNNNK
            return String.Format("{0," + length+":F" + Math.Max(length - 4, 0) + "}K", (Decimal)number/(1024));
        }

        if (number > 999) { // Over 999, value is NNNN.NNK
            return String.Format("{0," + length+":F" + Math.Max(length - 3, 0) + "}K", (Decimal)number/(1024));
        }
        
        return String.Format("{0," + length +"}B", number);
    }   
    public static byte centeredOffset(String message, int averageSize = 8) {
        return (byte)Math.Max(0, 64 - ((message.Length * averageSize) / 2));
    }
}