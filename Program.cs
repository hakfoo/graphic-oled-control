namespace graphic_oled_control;

using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO.Ports;
using System.Security.Principal;
using LibreHardwareMonitor.Hardware;


static class Program
{
    private static DisplayModule? display;
    private static int currentScreen = 0;
    private static int currentFrame = 0;
    private static Boolean advanceScreen = true;
    private static ContextMenuStrip? menu;
    private static List<DisplayScreen> screens = new List<DisplayScreen>();

    /// <summary>
    ///  The main entry point for the application.
    /// </summary>
    [STAThread]
    static void Main(String[] args)
    {
        String desiredPort = "";
        loadScreens();
        
        if (args.Length == 0) {
            String[] ports = SerialPort.GetPortNames();
            if(ports.Length != 1) {
                MessageBox.Show("No or Multiple serial ports detected; please pass the desired port name via command line");
                Application.Exit();
                return;
            } else {
                desiredPort = ports[0];
            }
        } else {
            desiredPort = args[0];
        }
        initializeTrayIcon(desiredPort);
        display = new DisplayModule(desiredPort);
        display.initialize();    
        Application.ApplicationExit += new EventHandler(exitHandler);

        System.Timers.Timer timer = new System.Timers.Timer(750);
        timer.Elapsed += advanceFrame;
        timer.Enabled = true;
        Application.Run();
        display.turnOffScreen();
    }

     private static void advanceFrame(Object? source, EventArgs? args) {    
        if (currentFrame >= screens[currentScreen].getLifespan()) {
            if (advanceScreen) {
                currentScreen++;
            }
            currentFrame = 0;
        }
        if (currentScreen >= screens.Count) {
            currentScreen = 0;
        }
        if (display is not null) {
            screens[currentScreen].RenderScreen(display, currentFrame);
        }
        currentFrame++;
    }

    private static void initializeTrayIcon(String desiredPort)
    {
        NotifyIcon icon = new NotifyIcon();
        icon.Icon = new Icon("./trayIcon.ico");
        menu = new ContextMenuStrip();
        ToolStripMenuItem menuItem;

        int i = 0;
        foreach (DisplayScreen screen in screens) {    
            menuItem = new ToolStripMenuItem("Skip to " + screen.getName());
            menuItem.Click += menuHandler;
            menu.Items.Add(menuItem);
            i++;
        }
        menuItem = new ToolStripMenuItem("Don't Rotate");
        menuItem.Click += dontRotateHandler;
        menu.Items.Add(menuItem);

        menuItem = new ToolStripMenuItem("&Quit");
        menuItem.Click += exitHandler;
        menu.Items.Add(menuItem);
        
        icon.Text = "OLED Control (" + desiredPort + ")";
        icon.ContextMenuStrip = menu;
        icon.Visible = true;   
    }

    private static void loadScreens() 
    {
        CPUScreen cpu = new CPUScreen();
        TimeScreen time = new TimeScreen();
        NetworkScreen network = new NetworkScreen();
        DiscScreen disc = new DiscScreen();
        screens.Add(time);
        screens.Add(cpu);
        screens.Add(network);
        screens.Add(disc);
        int gpus = 0;

        /* Meaningful access to sensors requires running as an admin */
        WindowsIdentity identity = WindowsIdentity.GetCurrent();
        WindowsPrincipal principal = new WindowsPrincipal(identity);
        if (principal.IsInRole(WindowsBuiltInRole.Administrator)) {
            CPUClockScreen cpuClock = new CPUClockScreen();
            screens.Add(cpuClock);
            TemperatureScreen temperature = new TemperatureScreen();
            screens.Add(temperature);
            Computer testComputer = HardwareSensor.getComputer();
            foreach (IHardware hardware in testComputer.Hardware) {
                if (hardware.HardwareType == HardwareType.GpuAmd || hardware.HardwareType == HardwareType.GpuNvidia) {
                    GPUScreen gpu = new GPUScreen(gpus);
                    screens.Add(gpu);
                    gpus++;
                }
            }
        }
    }

    private static void exitHandler(object? sender, EventArgs? e) 
    {
        display?.turnOffScreen();
        Application.Exit();
    }

    private static void dontRotateHandler(object? sender, EventArgs? e) 
    {
        advanceScreen = !advanceScreen;
        if (sender is not null) {
        ((ToolStripMenuItem)sender).Checked = !advanceScreen;
        }
    }

    private static void menuHandler(object? sender, EventArgs? e) 
    {
        int screen = 0;
        if (menu is not null) {
            foreach (ToolStripMenuItem item in menu.Items) {
                if (sender == item) {
                    currentScreen = screen;
                    currentFrame = -1;
                    advanceFrame(null, null);
                    return;
                }
                screen++;
            }
            
        }
    }
}