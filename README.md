# Graphic OLED Control

Simple Windows utility for driving a small serial display module with system stats

This is specifically designed around the Digole serial display module DS12864OLED-3W, using the USB-to-UART module they also sell.

## Configuration

You should be able to just build it with "dotnet build".  When ran, it launches with a tray menu that lets you skip to any of the existing screens, freeze on one of them instead of changing every few seconds, or close.  You may need to move the trayicon.ico file into whatever directory the binary lives in or is called from.

The USB-to-serial adaptor will mount itself at some random COM port.  The program assumes if there's one serial port Windows can find, it's the right one, but if you have multiple ports, or it can't detect properly, you can pass the port name (i. e. COM5) as a command line option when you run it.  The tooltip for the tray icon mentions the port name currently in use.

## Credits
Uses the LibreHardwareMonitor library (https://github.com/LibreHardwareMonitor/LibreHardwareMonitor) for extracting the CPU speed.  For some brilliant reason, there's no "OS-supplied" way to get a CPU clock that's remotely accurate with variable-clock CPUs.  See license of this library in the "LICENSE-for-librehardwaremonitor-library" file.