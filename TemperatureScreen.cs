using LibreHardwareMonitor.Hardware;

/* Uses UpdateVisitor class from the LibreHardwareMonitor project example
https://github.com/LibreHardwareMonitor/LibreHardwareMonitor - see LICENSE-for-librehardwaremonitor-library file
Data extraction also based on example shown on there.
*/

class TemperatureScreen : DisplayScreen
{
    public TemperatureScreen()
    {
    }


    public override void RenderScreen(DisplayModule display, int currentFrame)
    {
        Computer computer = HardwareSensor.getComputer();
        Dictionary<String, float?> fans = new Dictionary<string, float?> {};
        Dictionary<String, float?> temperatures = new Dictionary<string, float?> {};
        foreach (IHardware hardware in computer.Hardware) {
            if (hardware.HardwareType == HardwareType.Cpu || hardware.HardwareType == HardwareType.Motherboard || hardware.HardwareType == HardwareType.SuperIO) {
                hardware.Update();
                // The CPU temperatures are off the main CPU device, but motherboard temps and fans are
                // off a subdevice (mainboard) -> (monitoring chip) -> sensors
                
                foreach (ISensor sensor in hardware.Sensors) {
                    if (sensor.SensorType == SensorType.Temperature) {
                        temperatures.Add(sensor.Name, sensor.Value);
                    }
                    if (sensor.SensorType == SensorType.Fan) {
                        fans.Add(sensor.Name, sensor.Value);
                    }
                }
                foreach (IHardware subhardware in hardware.SubHardware) {
                        subhardware.Update();
                        foreach (ISensor sensor in subhardware.Sensors) {
                        if (sensor.SensorType == SensorType.Temperature) {
                            temperatures.Add(sensor.Name, sensor.Value);
                        }
                        if (sensor.SensorType == SensorType.Fan) {
                            fans.Add(sensor.Name, sensor.Value);
                        }
                
                    }
                }
            }
        }
        display.clearScreen();
        display.setFont(10);
        byte offset = 0;
        foreach (var obj in temperatures.OrderByDescending(x => x.Value)) {
            if (offset >= 6) break;
            display.setPosition(0, offset);
            String caption = obj.Key.Length > 6 ? obj.Key.Substring(0, 6) : obj.Key;
            display.writeText(String.Format("{0,-6} {1,3:F0}c", caption, obj.Value));
            offset++;
        }
        offset = 0;
        foreach (var obj in fans.OrderByDescending(x => x.Value)) {
            if (offset >= 6) break;
            display.setPosition(12, offset);
            String caption = obj.Key.Length > 4 ? obj.Key.Substring(0, 4) : obj.Key;
            display.writeText(String.Format("{0,-4} {1,4:F0}", caption, obj.Value));
            offset++;
        }

        display.setFont(0);
        display.dumpBuffer();
    }

    public override String getName()
    {
        return "Fans / CPU temp";
    }
}