using System.Diagnostics;

class TimeScreen : DisplayScreen
{
    private PerformanceCounter uptimeCounter;
    public TimeScreen()
    {
        uptimeCounter = new PerformanceCounter("System", "System Up Time");
        // This seems to initialize to zero for some reason, so prod it to avoid a flash
        uptimeCounter.NextValue();
    }
    public override void RenderScreen(DisplayModule display, int currentFrame)
    {
        String formatString, formattedString;
        DateTime current = DateTime.Now;
        String currentTime = current.ToString("HH:mm.ss");
        String currentDate = current.ToString("ddd. yyyy-MM-dd");
        float uptime = uptimeCounter.NextValue();
        int days = (int)Math.Floor(uptime / 86400);
        uptime -= days * 86400;
        int hours = (int)Math.Floor(uptime / 3600);
        uptime -= hours * 3600;
        int mins = (int)Math.Floor(uptime / 60);
        uptime -= mins * 60;

        display.clearScreen();
        display.setFont(120);
        display.setPosition(2, 0);
        display.writeText(currentTime);
        display.setFont(0);

        display.setPrecisePosition(4, 45);
        display.writeText(currentDate);
        display.setPrecisePosition(0, 60);
    
        if (days != 1) {
            formatString = "{0} days {1:00}:{2:00}.{3:00}";

        } else {
            formatString = "{0} day {1:00}:{2:00}.{3:00}";
        }
        formattedString = String.Format(formatString, days, hours, mins, uptime);
        
        display.setPrecisePosition(PrettyNumbers.centeredOffset(formattedString), 60);
        
    
        display.writeText(formattedString);
    
        display.dumpBuffer();
    }
    public override String getName()
    {
        return "Date/Time";
    }
}